import React, { Component } from 'react';
import cards from '../data/cards';

import {
    View,
    ActivityIndicator,
 } from 'react-native';

import SostyleCard from '../components/card';

import {
    Container,
    DeckSwiper,
} from 'native-base';

export default class MainScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            cards: [],
        };

        this.onSwipeLeft = this.onSwipeLeft.bind(this);
        this.onSwipeRight = this.onSwipeRight.bind(this);
        this.takeIt = this.takeIt.bind(this);
        this.leaveIt = this.leaveIt.bind(this);
    }

    getCards() {
        return cards;
    }

    onSwipeLeft({username}) {
        this.onSwipe(username, 0);
    }

    onSwipeRight({username}) {
        this.onSwipe(username, 1);
    }

    onSwipe(username, status) {
        const cards = this.state.cards.map(card => {
            if (card.username === username)
                card.status = status;
            return card;
        });
        this.setState({cards});
    }

    takeIt(username) {
        this._deckSwiper._root.swipeRight();
        this.onSwipeRight({username});
    }

    leaveIt(username) {
        this._deckSwiper._root.swipeLeft();
        this.onSwipeLeft({username});
    }

    componentDidMount() {
        this.setState({
            isLoading: false,
            cards: this.getCards(),
        });
    }

    render() {
        if(this.state.isLoading){
            return(
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator/>
                </View>
            )
        }
        return (
          <Container>
              {this.state.cards && this.state.cards.length > 0 &&
                <View>
                    <DeckSwiper
                        ref={(c) => this._deckSwiper = c}
                        dataSource={this.state.cards}
                        renderItem={item => {
                            return (
                                <SostyleCard
                                    key={item.username}
                                    item={item}
                                    takeIt={() => this.takeIt(item.username)}
                                    leaveIt={() => this.leaveIt(item.username)}
                                />
                            )
                        }}
                        onSwipeLeft={this.onSwipeLeft}
                        onSwipeRight={this.onSwipeRight}
                    />
                </View>}
          </Container>
        );
    }
}
