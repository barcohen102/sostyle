import React, { Component } from 'react';
import cards from '../data/cards';

import {
    View,
    ScrollView,
    CameraRoll,
    StyleSheet,
} from 'react-native';

import {
    Container,
    Thumbnail,
    Content,
    Footer,
    FooterTab,
    Button,
    Icon,
    Text,
} from 'native-base';


export class NavigationButtons extends Component {
    constructor(props) {
        super(props);

        this.state = {
            active: this.props.section,
        };

        this.setCamera = this.setCamera.bind(this);
        this.setGallery = this.setGallery.bind(this);
    }

    setCamera() {
        this.setState({active: 'camera'});
    }

    setGallery() {
        this.setState({active: 'gallery'});
    }

    render() {
        return (
            <Container>
                <Content/>
                <Footer>
                    <FooterTab>
                        <Button vertical onPress={this.setCamera}>
                            <Icon name="camera" style={[
                                this.state.active === 'camera'
                                    ? {opacity: 0.8, fontWeight: 'bold'}
                                    : {}
                            ]} />
                            <Text style={[
                                this.state.active === 'camera'
                                    ? {opacity: 0.8, fontWeight: 'bold'}
                                    : {}
                                ]}
                            >Camera</Text>
                        </Button>
                        <Button vertical onPress={this.setGallery}>
                            <Icon name="image" style={[
                                this.state.active === 'gallery'
                                    ? {opacity: 0.8}
                                    : {}
                            ]} />
                            <Text style={[
                                this.state.active === 'gallery'
                                    ? {opacity: 0.8}
                                    : {}
                            ]}
                            >Gallery</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

export class Camera extends Component {
    render() {
        return (
            <View>
            </View>
        );
    }
}

export class Gallery extends Component {
    constructor(props) {
        super(props);

        this.state = {
            photos: [],
        };

    }

    componentDidMount() {
        this.setState({photos: this.getPhotos()});
    }

    getPhotos() {
        return cards.map(card => card.image);
    }

    render() {
        return (
            <ScrollView style={{flex: 1, flexDirection: 'row'}}>
                {this.state.photos.map(photo => (
                    <View style={{margin: 5}} key={photo}>
                        <Thumbnail
                            key={photo}
                            square
                            large
                            source={photo}
                        />
                    </View>
                    )
                )}
            </ScrollView>
        );
    }
}

export default class UploadScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            section: 'gallery', // ['gallery', 'camera']
        };
    }

    async getPhotos() {
        const roll = await CameraRoll.getPhotos({
            first: 25,
            assetType: 'All',
        });
        this.setState({
            photos: roll.edges
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.container}>
                    {this.state.section === 'gallery' &&
                    <Gallery />}
                    {this.state.section === 'camera' &&
                    <Camera />}
                </View>
                <NavigationButtons active={this.state.section} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
