const cards = [
    {
        text: 'Guy Peer',
        username: '@guypeer8',
        likeText: 'TAKE IT',
        dislikeText: 'LEAVE IT',
        image: require('../../assets/images/guy.jpg'),
        status: null,
    },
    {
        text: 'Mr no-face',
        anonymous: true,
        username: '@anonymous',
        likeText: 'TAKE IT',
        dislikeText: 'LEAVE IT',
        image: require('../../assets/images/guy-ano.jpg'),
        status: null,
    },
    {
        text: 'Bar Cohen',
        username: '@barcohen10',
        likeText: 'TAKE IT',
        dislikeText: 'LEAVE IT',
        image: require('../../assets/images/bar.jpg'),
        status: null,
    },
    {
        text: 'Mr no-face',
        anonymous: true,
        username: '@anonymous',
        likeText: 'TAKE IT',
        dislikeText: 'LEAVE IT',
        image: require('../../assets/images/bar-ano.jpg'),
        status: null,
    },
    {
        text: 'Noa Paz',
        username: '@noasweet',
        likeText: 'TAKE IT',
        dislikeText: 'LEAVE IT',
        image: require('../../assets/images/noa.jpg'),
        status: null,
    },
    {
        text: 'Garry Livshits',
        username: '@garry233',
        likeText: 'TAKE IT',
        dislikeText: 'LEAVE IT',
        image: require('../../assets/images/garry.jpg'),
        status: null,
    }
];

export default cards;
