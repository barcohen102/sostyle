import React, { Component } from 'react';
import {
    Image
} from 'react-native';
import {
    Container,
    Header,
    Left,
    Body,
    Right,
    Button,
    Icon,
    Title
} from 'native-base';
export default class SostyleHeader extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Container style = {{ height: 100, flex: 0 }}>
                <Header style = {{ height: 100 }}>
                    <Left style={{flex:1, flexDirection: 'row', alignItems:'center'}}>
                        {this.props.screen !== 'main' &&
                        <Button transparent onPress={() => this.props.navigateTo('main')}>
                            <Icon name='arrow-back' />
                        </Button>}
                        <Image
                            style = {{ width: 130 }}
                            source={require('../../assets/images/sostyle-logo.png')}
                            resizeMode = 'contain'
                        />
                    </Left>
                    <Right>
                        <Button transparent>
                          <Icon name='search' />
                        </Button>
                        <Button transparent>
                          <Icon name='heart' />
                        </Button>
                        <Button transparent>
                          <Icon name='more' />
                        </Button>
                    </Right>
                </Header>
            </Container>
        );
    }
}
