import React, {Component} from 'react';
import {
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import {
    Card,
    CardItem,
    Left,
    Thumbnail,
    Text,
    Icon,
    Body,
    Button,
} from 'native-base';

export default class SostyleCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {item} = this.props;
        return (
            <Card style={[styles.card,
                    (item.status === 1
                        ? {borderWidth: 30, borderColor: 'green'}
                        : (
                            item.status === 0
                            ? {borderWidth: 30, borderColor: 'red'}
                            : {}
                        )
                    )
                ]
            }>
                {item.anonymous &&
                <Text style={styles.anonymousText}>A n o n y m o u s</Text>}
                <CardItem>
                    <Left>
                        <Thumbnail source={item.image} />
                        <Body>
                            <Text>{item.text}</Text>
                            <Text note>{item.username}</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem cardBody>
                    <Image
                        style={{ height: 300, flex: 1 }}
                        source={item.image}
                    />
                </CardItem>
                <View style={styles.emotionButtons}>
                    <CardItem style={styles.emotionButton}>
                        <TouchableOpacity style={styles.emotionButton} onPress={this.props.leaveIt}>
                            <Text style={styles.emotionIconNegative}>{item.dislikeText}</Text>
                            <Icon name="trash" style={styles.trashIcon} />
                        </TouchableOpacity>
                    </CardItem>
                    <CardItem style={styles.emotionButton}>
                        <TouchableOpacity style={styles.emotionButton} onPress={this.props.takeIt}>
                            <Icon name="heart" style={styles.heartIcon} />
                            <Text style={styles.emotionIconPositive}>{item.likeText}</Text>
                        </TouchableOpacity>
                    </CardItem>
                </View>
            </Card>
        );
    }
}

const styles = StyleSheet.create({
    cardStyle: {
        elevation: 3,
    },
    anonymousText: {
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 30,
        fontFamily: 'Baskerville-Bold',
    },
    emotionButtons: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    emotionButton: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    emotionIconPositive: {
        color: 'green',
    },
    emotionIconNegative: {
        color: 'red',
    },
    heartIcon: {
        color: 'green',
        fontSize: 24,
    },
    trashIcon: {
        color: 'red',
        marginLeft: 15,
        fontSize: 24,
    }
});
