import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    CameraRoll,
} from 'react-native';

import MainService from './src/services/main.service';
import MainScreen from './src/screens/main.screen';
import UploadScreen  from './src/screens/upload.screen';
import SostyleHeader from './src/components/header';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
        loaded: false,
        screen: 'main', // main, upload
    };
    this.onLoaded();
    this.navigateTo = this.navigateTo.bind(this);
  }

  onLoaded() {
    MainService.load(() => this.setState({
        loaded: true,
        screen: 'main',
    }));
  }

  navigateTo(screen) {
      this.setState({ screen });
  }

  render() {
    if (!this.state.loaded) {
      return (
        <View style={styles.loading}>
          <Image source={require('./assets/images/sostyle-logo.png')} resizeMode = 'cover' />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <SostyleHeader navigateTo={this.navigateTo} screen={this.state.screen} />
          {
            this.state.screen === 'main' &&
              <View style={styles.container}>
                  <MainScreen />
                  <TouchableOpacity style={styles.sosButton} onPress={() => this.navigateTo('upload')}>
                      <Text style={styles.sosButtonText}>SOS</Text>
                  </TouchableOpacity>
              </View>
          }
          {
            this.state.screen === 'upload' &&
                <View style={styles.container}>
                    <UploadScreen />
                </View>
          }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  loading: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    alignItems: 'center',
    justifyContent: 'center'
  },
  header: {
    backgroundColor: '#3e113c',
  },
  sosButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f24d5f',
    height: 80
  },
  sosButtonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 28
  }
});
